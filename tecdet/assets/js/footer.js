﻿document.write('\
  <!DOCTYPE html>\
  <html lang="pt-br">\
  \
  <head>\
    <meta charset="utf-8">\
    <meta content="width=device-width, initial-scale=1.0" name="viewport">\
    \
    <title>Footer</title>\
    <meta content="" name="description">\
    <meta content="" name="keywords">\
    \
    <!-- Vendor CSS Files -->\
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">\
    <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">\
    \
    <!-- Template Main CSS File -->\
    \
    <script\
    src="https://code.jquery.com/jquery-1.12.4.js">\
    </script>\
    \
    <script>\
      $(function () {\
          $("#header").load("header.html");\
          $("#footer").load("footer.html");\
      });\
    </script>\
    <link href="assets/css/style.css" rel="stylesheet">\
  </head>\
  \
  <body>\
      <footer id="footer" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">\
          <div class="footer-top">\
            <div class="container">\
              <div class="row">\
              \
                <div class="col-lg-3 col-md-6 footer-links">\
                  <h4>Links Úteis</h4>\
                  <ul>\
                    <li><i class="bx bx-chevron-right"></i> <a href="index.html">Início</a></li>\
                    <li><i class="bx bx-chevron-right"></i> <a href="empresa.html">Empresa</a></li>\
                    <li><i class="bx bx-chevron-right"></i> <a href="services.html">Serviços</a></li>\
                    <li><i class="bx bx-chevron-right"></i> <a href="contato.html">Contato</a></li>\
                    <li><i class="bx bx-chevron-right"></i> <a href="Login.html">Área do Cliente</a></li>\
                  </ul>\
                </div>\
                \
                <div class="col-lg-3 col-md-6 footer-links">\
                  <h4>Nossos Serviços</h4>\
                  <ul>\
                    <li><i class="bx bx-chevron-right"></i> <a href="services.html#publico">Setor Público</a></li>\
                    <li><i class="bx bx-chevron-right"></i> <a href="services.html#privado">Setor Privado</a></li>\
                  </ul>\
                </div>\
                \
                <div class="col-lg-3 col-md-6 footer-contact">\
                  <h4>Contato</h4>\
                  <p>\
                    Av. Dr. Tancredo de Neves, 110 <br>\
                    Bairro: Jardim Santa Rita de Cassia <br>\
                    Cidade: Bragança Paulista - SP  <br>\
                    CEP: 12914-160 <br> <br>\
					<b>CNPJ:</b> 03.390.087/0001-49<br>\
                    <b>Telefone:</b> (11) 4032-7068<br>\
                    <b>E-mail:</b> tecdet@tecdet.com.br<br>\
                  </p>\
                \
                </div>\
                \
                <div class="col-lg-3 col-md-6 footer-info">\
                  <h3>Redes Sociais</h3>\
                  <p>TECDET - A mais de 20 anos de conhecimento e experiência no ramo de trânsito</p>\
                  <div class="social-links mt-3">\
                    <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>\
                    <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>\
                    <a href="https://br.linkedin.com/company/tecdet-tecnologia-em-deteccoes-comercio-importacao-e-exportacao" class="linkedin"><i class="bx bxl-linkedin"></i></a>\
                  </div>\
                </div>\
              \
              </div>\
            </div>\
          </div>\
          <div class="container">\
            <div class="copyright">\
              &copy; Copyright <strong><span>TECDET</span></strong>. All Rights Reserved\
            </div>\
          </div>\
        </footer><!-- End Footer -->\
  </body>\
  </html>\
');