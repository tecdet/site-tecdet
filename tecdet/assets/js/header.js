document.write('\
<html>\
<head>\
  <link href="assets/img/favicon.png" rel="icon">\
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">\
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">\
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">\
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">\
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">\
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">\
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">\
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">\
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">\
  <link href="assets/css/style.css" rel="stylesheet">\
</head>\
<body>\
<header id="header" class="fixed-top d-flex align-items-center">\
    <div class="container d-flex justify-content-between align-items-center">\
      <div class="logo">\
         <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>\
      </div>\
      <nav id="navbar" class="navbar">\
        <ul>\
          <li><a href="index.html">Início</a></li>\
          <li><a href="empresa.html">Empresa</a></li>\
          <li><a href="services.html">Serviços</a></li>\
          <li><a href="contato.html">Contato</a></li>\
          <li><a href="login.html">Login</a></li>\
        </ul>\
        <i class="bi bi-list mobile-nav-toggle"></i>\
      </nav>\
    </div>\
    <script>\
    var curentLocation = location.href;\
        if(curentLocation.indexOf("#")>0)\
        {\
           curentLocation = curentLocation.substring(0, curentLocation.indexOf("#"));\
        }\
        if(curentLocation.indexOf(".html") == -1){\
          curentLocation = curentLocation +"\index.html";\
        }\
		const menuItem = document.querySelectorAll("a");\
		const menuLength = menuItem.length;\
			for(let i =0; i < menuLength; i++){\
				if(menuItem[i].href === curentLocation){\
					menuItem[i].className = "active";\
				}\
			}\
	</script>\
  </header>\
</body>\
</html>\
');

