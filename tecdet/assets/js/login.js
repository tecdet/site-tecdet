﻿function Login() {
    var done = 0;
    var usuario = document.getElementsByName('email')[0].value;
    usuario = usuario.toLowerCase();
    var senha = document.getElementsByName('pass')[0].value;

    var dados = { "Nome": usuario, "Senha": senha };

    if (usuario == null || senha == null) {
        alert("Usuário ou senha não digitado.");
    }

    var json = JSON.stringify(dados);
 
    var ajax = new XMLHttpRequest();

    ajax.open("POST", "http://bragancatecdet.ddns.net/APITecdetSiteOficial/api/Usuario", true);

    ajax.send(json);

    //alert("ESPERE");

    ajax.onreadystatechange = () => {
        if (ajax.readyState == 4 && ajax.status == 202) {
            var data = ajax.responseText;
            var json = JSON.parse(data);
            var link = json["Link"];
            var tipoAcesso = json["TipoAcesso"];
            var nome = json["Nome"];
            var token = json["Token"];
            var senha = json["Senha"];

            var parametros = "?nome:" + nome + "&senha:" + senha + "&token:" + token;
            var token = "&token:" + token;

            if (token == null) {
                alert("Usuário ou senha invalido!");
            }

            if (tipoAcesso == "Externo") {
                window.location.href = link + parametros;
            }
            else {
                sessionStorage.setItem('parametros', parametros);
                sessionStorage.setItem('token', token);
                window.location.href = "acesso-interno.html";
            }
        }

        if (ajax.readyState == 4 && ajax.status == 403) {
            var data = ajax.responseText;
            var json = JSON.parse(data);
            var erro = json["Erro"];

            if (erro != null) {
                alert(erro);
                document.getElementsByName('email')[0].value = '';
                document.getElementsByName('pass')[0].value = '';
            }
        }
    }
}
