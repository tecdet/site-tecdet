﻿<?php
session_start();
$nome = $_POST["name"];
$email = $_POST["email"];
$titulo = $_POST["subject"];
$mensagem = $_POST["message"];
$dataHora = date('d/m/Y H:i');
$captchaDigitado = $_POST["txtInput"];
$captcha = $_POST["mainCaptcha2"];

$page = $_SERVER['PHP_SELF'];

function ArquivoSenha(){
	$arquivo = "info.txt";
	//Variável $fp armazena a conexão com o arquivo e o tipo de ação.
	$fp = fopen($arquivo, "r");
	//Lê o conteúdo do arquivo aberto.
	$conteudo = fread($fp, filesize($arquivo));
	$conteudo = substr($conteudo,strrpos($conteudo,":") + 1, 24);
	return $conteudo;
	//Fecha o arquivo.
	fclose($fp);
}
require_once("PHPMailer-master/PHPMailerAutoload.php");

if($captcha == $captchaDigitado){

	$mail = new PHPMailer();
	$mail->isSMTP();
	$mail->Host = 'smtp.tecdet.com.br';
	$mail->Port = 587;
	$mail->SMTPAutoTLS = false;
	$mail->SMTPSecure = false;
	$mail->SMTPAuth = true;
	$mail->CharSet = 'UTF-8';
	$mail->Username = "site@tecdet.com.br";
	$mail->Password = ArquivoSenha();
	$mail->setFrom("site@tecdet.com.br", $nome);
	$mail->addAddress("site@tecdet.com.br");
	$mail->Subject = "E-mail TECDET Pedido de Contato - ".$titulo;
	$mail->msgHTML("
				<style type='text/css'>
				p {
				margin:1px;
				font-family:Arial;
				font-size:24px;
				color: black;
				text-align: left;
				}
				</style>
			<html>
				<p><b>Nome:</b> {$nome}</p><br/>
				<p><b>E-mail:</b> {$email}</p><br/>
				<p><b>Mensagem:</b> {$mensagem}</p><br/>
				<p><b>Data e hora do contato:</b> $dataHora </p>
			</html>
	");
	$mail->AltBody = "de: {$nome}\nemail:{$email}\nmensagem: {$mensagem}";
	
	if($mail->send()) {
		echo 'OK';
	} else {
		echo 'Não foi possível enviar o formulário!';
	}
}else{
	echo 'Digite o valor correto da imagem!';
}

?>